﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    internal class PartnerBuilder
    {
        private Partner _partner;

        public PartnerBuilder()
        {
            _partner = new Partner();
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
        }

        public PartnerBuilder WithPartnerLimit(PartnerPromoCodeLimit limit)
        {
            _partner.PartnerLimits.Add(limit);

            return this;
        }

        public PartnerBuilder WithIssuedPromoCodes(int promoCodesCount)
        {
            _partner.NumberIssuedPromoCodes = promoCodesCount;

            return this;
        }

        public PartnerBuilder Active(bool isActive)
        {
            _partner.IsActive = true;

            return this;
        }

        public PartnerBuilder NotActive()
        {
            _partner.IsActive = false;

            return this;
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}
