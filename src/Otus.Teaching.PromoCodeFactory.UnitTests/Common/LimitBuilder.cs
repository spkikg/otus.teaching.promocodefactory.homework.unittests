﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    internal class LimitBuilder
    {
        PartnerPromoCodeLimit _limit;

        public LimitBuilder()
        {
            _limit = new PartnerPromoCodeLimit();
        }

        public LimitBuilder WithEndDate(DateTime date)
        {
            _limit.EndDate = date;

            return this;
        }

        public LimitBuilder WithLimitNumber(int limit)
        {
            _limit.Limit = limit;

            return this;
        }

        public LimitBuilder WithPartner(Partner partner)
        {
            _limit.Partner = partner;

            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return _limit;
        }
    }
}
