﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services;
using Otus.Teaching.PromoCodeFactory.UnitTests.Factories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests 
    {
        private readonly IFixture _fixture;
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnerRepoMock;
        private readonly Mock<IPartnerLimitService> _limitServiceMock;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _limitServiceMock = new Mock<IPartnerLimitService>();
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepoMock = new Mock<IRepository<Partner>>();
            _fixture.Inject(_partnerRepoMock);
            _fixture.Inject(_limitServiceMock);
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_NotFoundCodeResult()
        {
            // Arrange
            var limitRequest = CreatePromoCodeRequest(5, DateTime.MaxValue);

            _partnerRepoMock.Setup(rep => rep.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(null as Partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), limitRequest);

            // Assert
            result.Should().BeOfType<Microsoft.AspNetCore.Mvc.NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotActive_BadRequestResult()
        {
            // Arrange 
            var partnerNotActive = new PartnerBuilder().NotActive().Build();
            var limitRequest = CreatePromoCodeRequest(5, DateTime.MaxValue);

            _partnerRepoMock.Setup(rep => rep.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partnerNotActive);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), limitRequest);

            // Assert
            result.Should().BeOfType<Microsoft.AspNetCore.Mvc.BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetPartnerLimit_OldLimitsReset()
        {
            // Arrange 
            var partner = PartnerFactory.CreatePartnerWithNotHitLimit();
            var newLimit = LimitFactory.CreateActive(partner);
            var limitRequest = CreatePromoCodeRequest(5, DateTime.MaxValue);

            _partnerRepoMock.Setup(rep => rep.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            _partnerRepoMock.Setup(rep => rep.UpdateAsync(It.IsAny<Partner>()));

            _limitServiceMock.Setup(serv => serv.ResetPartnerLimit(It.IsAny<Partner>()));
            _limitServiceMock.Setup(serv => serv.CreatePartnerLimit(It.IsAny<Partner>(), It.IsAny<int>(), It.IsAny<DateTime>())).Returns(newLimit);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), limitRequest);

            // Assert
            _limitServiceMock.Verify(m => m.ResetPartnerLimit(It.IsAny<Partner>()), Times.Once());
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetPartnerLimit_LimitSavedToDb()
        {
            // Arrange 
            var partner = PartnerFactory.CreatePartnerWithNotHitLimit();
            var limitRequest = CreatePromoCodeRequest(5, DateTime.MaxValue);
            var newLimit = LimitFactory.CreateActive(partner);

            _partnerRepoMock.Setup(rep => rep.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            _partnerRepoMock.Setup(rep => rep.UpdateAsync(It.IsAny<Partner>()));

            _limitServiceMock.Setup(serv => serv.ResetPartnerLimit(It.IsAny<Partner>()));
            _limitServiceMock.Setup(serv => serv.CreatePartnerLimit(It.IsAny<Partner>(), It.IsAny<int>(), It.IsAny<DateTime>())).Returns(newLimit);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), limitRequest);

            // Assert
            _partnerRepoMock.Verify(m => m.UpdateAsync(It.IsAny<Partner>()), Times.Once());
        }

        private SetPartnerPromoCodeLimitRequest CreatePromoCodeRequest(int limit, DateTime endDate)
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                EndDate = endDate,
                Limit = limit
            };
        }
    }
}