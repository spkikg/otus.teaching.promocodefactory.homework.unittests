﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using namespace_Otus.Teaching.PromoCodeFactory.Factories;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class PartnerLimitFactoryTests
    {
        private readonly IFixture _fixture;
        private readonly PartnerLimitFactory _limitFactory;

        public PartnerLimitFactoryTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _limitFactory = _fixture.Build<PartnerLimitFactory>().OmitAutoProperties().Create();
        }

        [Fact]
        public void PartnerLimitFactory_CreatePartnerLimit_LimitCreated()
        {
            // Arrange
            var partner = PartnerFactory.CreatePartnerWithNotHitLimit();

            // Act
            var createDate = DateTime.Now;
            var endDate = DateTime.MaxValue;
            var limit = _limitFactory.Create(partner, 5, createDate, endDate);

            // Assert
            limit.Partner.Should().Be(partner);
            limit.CancelDate.HasValue.Should().Be(false);
            limit.PartnerId.Should().Be(partner.Id);
            limit.Limit.Should().Be(5);
            limit.EndDate.Should().Be(endDate);
            limit.CreateDate.Should().Be(createDate);
        }
    }
}
