﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Factories
{
    internal static class LimitFactory
    {
        public static PartnerPromoCodeLimit CreateActive(Partner partner)
        {
            return new LimitBuilder()
                   .WithPartner(partner)
                   .WithEndDate(DateTime.MaxValue)
                   .WithLimitNumber(5)
                   .Build();
        }
    }
}
