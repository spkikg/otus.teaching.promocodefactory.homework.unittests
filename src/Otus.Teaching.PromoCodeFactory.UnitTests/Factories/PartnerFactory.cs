﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    internal static class PartnerFactory
    {
        public static Partner CreatePartnerWithNotHitLimit()
        {
            var limit = new LimitBuilder().
                   WithEndDate(DateTime.MaxValue).
                   WithLimitNumber(5).
                   Build();

            return new PartnerBuilder().Active(true).WithPartnerLimit(limit).WithIssuedPromoCodes(2).Build();
        }

        public static Partner CreatePartner(int issuedNumber, int limitNumber)
        {
            var limit = new LimitBuilder().
                   WithEndDate(DateTime.MaxValue).
                   WithLimitNumber(limitNumber).
                   Build();

            return new PartnerBuilder().Active(true).WithPartnerLimit(limit).WithIssuedPromoCodes(issuedNumber).Build();
        }

        public static Partner CreatePartnerWithHitLimit()
        {
            var limit = new LimitBuilder().
                   WithEndDate(DateTime.MaxValue).
                   WithLimitNumber(5).
                   Build();

            return new PartnerBuilder().Active(true).WithPartnerLimit(limit).WithIssuedPromoCodes(5).Build();
        }


    }
}
