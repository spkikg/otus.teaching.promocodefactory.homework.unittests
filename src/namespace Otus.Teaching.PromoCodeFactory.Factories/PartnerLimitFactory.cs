﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace namespace_Otus.Teaching.PromoCodeFactory.Factories
{
    public class PartnerLimitFactory : IPartnerLimitFactory
    {
        public PartnerPromoCodeLimit Create(Partner partner, int limit, DateTime createDate, DateTime endDate)
        {
            return new PartnerPromoCodeLimit
            {
                Partner = partner,
                CreateDate = createDate,
                EndDate = endDate,
                Id = Guid.NewGuid(),
                Limit = limit,
                PartnerId = partner.Id
            };
        }
    }
}
