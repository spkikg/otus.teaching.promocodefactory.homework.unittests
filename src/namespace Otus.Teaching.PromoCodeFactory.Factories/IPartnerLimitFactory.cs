﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace namespace_Otus.Teaching.PromoCodeFactory.Factories
{
    public interface IPartnerLimitFactory
    {
        PartnerPromoCodeLimit Create(Partner partner, int limit, DateTime createDate, DateTime endDate);
    }
}
