﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Services
{
    public interface IPartnerLimitService
    {
        void ResetPartnerLimit(Partner partner);
        PartnerPromoCodeLimit CreatePartnerLimit(Partner partner, int limit, DateTime endDate);
    }
}
