﻿using namespace_Otus.Teaching.PromoCodeFactory.Factories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Services
{
  
    public class PartnerLimitService : IPartnerLimitService
    {
        private IPartnerLimitFactory _partnerLimitFactory;

        public PartnerLimitService(IPartnerLimitFactory partnerLimitFactory)
        {
            _partnerLimitFactory = partnerLimitFactory;
        }

        public PartnerPromoCodeLimit CreatePartnerLimit(Partner partner, int limit, DateTime endDate)
        {
            if (limit <= 0)
            {
                throw new ArgumentException("Лимит должен быть больше 0");
            }

            var newLimit = _partnerLimitFactory.Create(partner, limit, DateTime.Now, endDate);

            partner.PartnerLimits.Add(newLimit);

            return newLimit;
        }

        public void ResetPartnerLimit(Partner partner)
        {
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                    !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                if (partner.NumberIssuedPromoCodes != activeLimit.Limit)
                {
                    partner.NumberIssuedPromoCodes = 0;
                }

                activeLimit.CancelDate = DateTime.Now;
            }
        }
    }
    
}
